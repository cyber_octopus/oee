package visualization;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import startup.Constants;
import startup.Starter.LifeRunnable;
import animals.IndividualV1;
import communication.MyLog;

/**
 * Graphic panel
 * The field is an array.
 * @author lana
 *
 */
public class Display extends JFrame {

		private static final long serialVersionUID = 1579747902278268747L;
		MyLog mLog = new MyLog("Display", true);
		
		//surface to be drawn on
		Surface s; 
		String name;
		///
		LifeRunnable lifer;
		int step = 0;
		
		/**
		 * 
		 * @param n name of window;
		 */
		public Display(String n) {
			name = n;
	        initUI();
	        this.setVisible(true);
	    }

		public Display(LifeRunnable l) {
			name = "Open Ended Evolution";
			lifer = l;
	        initUI();
	        this.setVisible(true);
	    }
		
	    private void initUI() {
	    	
	        setTitle(name);
	        s = new Surface();
	        add(s);
	        int width = s.getWidth();
	        int length = s.getLength();
	        setSize(width,length);
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        setLocationRelativeTo(null);
	        
	        //refresh
	        int delay = Constants.refresh_rate; //milliseconds

	        ActionListener taskPerformer = new ActionListener() {
	          public void actionPerformed(ActionEvent evt) {
	        	Thread t = new Thread(new Runnable() {
					public void run() {
			        	s.repaint();
					}
				});
	        	t.start();
	          }
	        };

	        //new Timer(delay, taskPerformer).start();
	    }
	    
	    /**
	     * Add a object to be drawn on the pannel.
	     * @param c the object implementing the component interface
	     */
	    public void addComponent(GraphicalComponent c){
	    	s.addComponent(c);
	    }

	    
	    //hem ?
	    public JPanel getSurface(){
	    	return s;
	    }

	    /**
	     * removes from surface if exists
	     * @param c
	     */
		public void removeComponent(GraphicalComponent c) {
			s.removeComponent(c);
		}

		public void setConcurrentThread(LifeRunnable life) {
			lifer = life;
			s.setConcurrent(lifer);
		}

		public void updateComponents() {
			s.updateComponents();
			if(step%2==0){
				step = 0;
				Thread t = new Thread(new Runnable() {
					public void run() {
			        	s.repaint();
					}
				});
	        	t.start();
			}
			step++;
			//mLog.say("step "+step);
		}
	    
	    /**
	     * Add an object to be controlled by keyboard actions.
	     * @param p the object to be controlled
	     * @param string a unique name
	     */
}
	

	/**
	 * The surface on which we draw the graphics.
	 * @author lana
	 *
	 */
	class Surface extends JPanel{
		MyLog mLog = new MyLog("Surface", true);

		//size
		int w = 1200;
		int h = 1200;
        //grid step size
        int step = Constants.GridStep;
        boolean pause = false;
		LifeRunnable lifer = null;

		boolean busy = false;

		//list of things to draw
		public List<GraphicalComponent> components = new ArrayList<GraphicalComponent>();
		public List<GraphicalComponent> oldcomponents = new ArrayList<GraphicalComponent>();

		
		
		public void setConcurrent(LifeRunnable l) {
			lifer	= l;		
		}


		public void updateComponents() {
			if(!pause){
				busy = true;
				//mLog.say("cp "+components.size());
				oldcomponents = new ArrayList<GraphicalComponent>();
				int csize = components.size();
				if(csize>30000){
					oldcomponents.addAll(components.subList(0, 15000));
					List<GraphicalComponent> others = components.subList(15000, csize);
					Collections.shuffle(others);
					oldcomponents.addAll(others.subList(0, 15000));
				}else{
					oldcomponents.addAll(components);
				}
				busy = false;
			}
		}


	    /**
	     * Adds a object to be drawn on the pannel.
	     * @param c the object implementing the component interface
	     */
	    public void addComponent(GraphicalComponent c){
	    	components.add(c);
	    }
	    
	    public void removeComponent(GraphicalComponent c) {
			if(components.contains(c)){
				components.remove(c);
			}
		}
	    
		private static final long serialVersionUID = 6523850037367826272L;

		/**
		 * Sets the background grid.
		 * @param g
		 */
		private void init(Graphics g) {
			this.setOpaque(true);
			this.setBackground(Color.white);
			
	        Graphics2D g2d = (Graphics2D) g;
	        
	        //draw the coordinates lines
	        g2d.setColor(Color.black);
	        

	        for(int i=0;i<w;i+=step){
	        	//horizontal lines
	        	g2d.drawLine(0,i,w,i);
	        	//vertical lines
	        	g2d.drawLine(i,0,i,w);
	        }
		}

	    @Override
	    public void paintComponent(Graphics g) {

	        super.paintComponent(g);
	        init(g);
	        
	        if(Constants.draw){
	        	
	        	if(!busy){
	        		pause = true;
			        for(int i=0;i<oldcomponents.size();i++){
			        	if(Constants.uniformDouble()<Constants.draw_coarse){
			        		GraphicalComponent c = oldcomponents.get(i);
			        		if(c!=null)
			        			c.draw(g,step);
			        	}
			        }
			        pause = false;
	        	}
    		}
	    }
	    
	    public int getWidth(){
	    	return w;
	    }
	    
	    public int getLength(){
	    	return h;
	    }
	    
	    public int getStep(){
	    	return step;
	    }
		   
	}
	
	
