package communication;

import java.awt.Color;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import animals.EmbodiedIndividual;
import animals.Individual;
import animals.Node;
import animals.Tree;
import communication.Map.Cell;
import startup.Constants;
import visualization.Display;

public class Map {
	MyLog mlog = new MyLog("map", true);
	/**graphic panel*/
	Display d;
	/** 2D map is made of cells, in each cell there are creatures;*/
	Cell[][] map;
	/** width and length are the same*/
	int map_size;
	/** global var: id & number of animals until now*/
	int globalID = 0;
	/** data recording*/
	FileWriter summaryWriter;
	/** simulation time*/
	int time = 0;
	
	//for updates
	/** stores newly created individuals*/
	LinkedList<EmbodiedIndividual> babies;
	/** temporarily stores dead individuals*/
	LinkedList<EmbodiedIndividual> remove;
	/** idividuals that have moved*/
	LinkedList<EmbodiedIndividual> moving;
	/** positions of moved individuals */
	LinkedList<Double> newPositions;
	
	public Map(int mapSize, Display d){
		this.d  = d;
		map_size = mapSize;		
		//create map
		map = new Cell[map_size][map_size];
		babies = new LinkedList<EmbodiedIndividual>();
		remove = new LinkedList<EmbodiedIndividual>();
		moving = new LinkedList<EmbodiedIndividual>();
		newPositions = new LinkedList<Double>();
		//create cells
		for(int i=0;i<map_size;i++){
			for(int j=0;j<map_size;j++){
				map[i][j] = new Cell();
				/*if(j<map_size/2){
					map[i][j].ntransparency = 0.1;
				}
				if(i<map_size/2){
					map[i][j].ndensity = 0.9;
				}*/
			}
		}
		
		//writing data
		FileBuilder fb = new FileBuilder();
		summaryWriter = fb.getFileWriter();
		fb = null;
		//csv file header
		String str = "ID,parent,created,lifeSpan,speed,maxEnergy,kidEnergy,sensors,ancestor,nkids,pgmDeath, matForKids,"
				+ "luminosity,warm,loud,smelly,electric,eaten,complexPreys,nSymbionts,topSymbiont\n";
		//"ID,parent,created,lifeSpan,speed,maxEnergy,kidEnergy,sensors,ancestor,nkids,pgmDeath\n";
		//Parents and kids have the same ID.
    	try {
			summaryWriter.append(str);
			summaryWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addIndividual(int x, int y, EmbodiedIndividual i){
		Cell c = map[x][y];
		i.setCellTransparency(c.transparency);
		if(c.creatures.contains(i)){
			throw new Error("adding a creature already present in this cell ");
		}
		c.creatures.add(i);
	}
	
	public void removeIndividual(EmbodiedIndividual i){
		double[] position = i.getPosition();
		int x = (int)(position[0] +0.5);
		int y = (int)(position[1] +0.5);
		Cell c = map[x][y];
		
		int pos = c.creatures.indexOf(i);
		if(pos>=0){
			c.creatures.remove(pos);
		}else{
			//throw new Error("removing inexistent creature. Check flow ");
		}
	}
	
	/**
	 * 
	 * @param x1 old x
	 * @param y1 old y
	 * @param x2 new x
	 * @param y2 nex y
	 * @param i individual
	 */
	public void updatePosition(int x2, int y2, EmbodiedIndividual i){
		removeIndividual(i);
		addIndividual(x2,y2,i);		
	}
	
	/**
	 * @param x coordinate
	 * @param y coordinate
	 */
	public void updateCell(int x, int y){
		
		Cell c = map[x][y];
		c.calculateValues();
		int size = c.creatures.size();

		if(size==0){
			return;
		}
		
		
		//interactions: max is everyone interacts with everyone
		ArrayList<Integer> interacting = new ArrayList<Integer>();
		ArrayList<Integer> interactedOn = new ArrayList<Integer>();
		ArrayList<Integer> interaction = new ArrayList<Integer>();
		
		for (int i = 0; i < size; i++) {
			EmbodiedIndividual creature = c.creatures.get(i);
			double np[] = new double[2];
            boolean moved = false;
            double modSpeed = 0;
            double[] position = creature.getPosition();
        	int numberActions = 0;
            creature.exc_energy = 0;
            
			if(!creature.isLight() && creature.topSymbiont!=null){
            	//move
            	modSpeed = Constants.shiftMax(creature.getSpeed()*1.0/Constants.SpeedMax, c.transparency);
        		for(int j=0;j<2;j++){
        			if(generateBool()){
        				np[j]= position[j]+(modSpeed*Constants.SpeedFactor);//(creature.getSpeed()*Constants.SpeedFactor*c.density*c.transparency);
        			}else{
        				np[j] = position[j]-(modSpeed*Constants.SpeedFactor);
        			}
        			if(np[j]<0) np[j]=0;
        			if(np[j]>=map_size-0.5) np[j] = map_size-1;//for int truncated
        			if(np[j] != position[j]){
        				moved = true;
        				//mlog.say("moved "+position[j]+ " " + np[j]);
        			}
        		}
        		//costs energy
            	//modSpeed = shiftMax(creature.getSpeed()*1.0/Constants.SpeedMax, c.density);
				double energy = creature.getEnergy() 
						- Math.pow(creature.getSpeed(),2)*Constants.SpeedCost; //modSpeed
						
				if(energy<=0){
					remove.add(creature);
					continue;
				}else{
					creature.setEnergy(energy);
				}
        	}
            boolean alive = creature.update(babies, time, c.transparency);
           
            if(!alive){
            	remove.add(creature);
            } else{
        		Tree sensors = creature.getSensors();
        		
        		//interactions between creatures
                Node s = sensors.root;
                //iterate on properties
                ArrayList<Node> sChildren = s.getChildren();
                
                for(int k=0; k<sChildren.size();k++){
                	//this is the property
                	Node prop = sChildren.get(k);
                	//these are the value-action pairs
                	ArrayList<Node> pChildren = prop.getChildren();
                	
                	for(int l=0; l<pChildren.size();l++){
                		int value = pChildren.get(l).data;
                		//actions
        				ArrayList<Node> actions = pChildren.get(l).getChildren();
        				for (Iterator<Node> iterator = actions.iterator(); iterator.hasNext();) {
							Node node = iterator.next();
							//action
							int act = node.data;
							//interactions with other creatures
							
							if(act<3){
								//iterate creatures on this cell
		                		for(int m=0; m<c.creatures.size();m++){
		                			
		                			//creature can't interact on itself
		                			if(m==i){
		                				continue;
		                			}
		                			
		                			//only allow interacting with some of the creatures
		                			if(act==Constants.ActEat){
			                			double p = 2.0/(double)c.creatures.size();
			                			if(Constants.uniformDouble()>p){
			                				continue;
			                			}
		                			}
		                			
		                			EmbodiedIndividual cr2 = c.creatures.get(m);
		                			if(remove.contains(c.creatures.get(m)) | (cr2.isLight()) | interactedOn.contains(cr2)){
		                				continue;
		                			}

		                			if(act == Constants.ActIntegrate && creature.from_symbionts.size()>=creature.n_symbionts){
		                				continue;
		                			}
		                			
		                			//based on direct perception of other creatures properties
		                			/*int[] properties = cr2.getProperties();
		                			if(value == properties[k]){
		                				if(Constants.uniformDouble()>0.6){
		                					mlog.say("eat; property " + prop.data + " value "+value);
		                				}
		                				//record interaction
		                				interacting.add(i);
		                				interactedOn.add(m);
		                				//get random action
		                				ArrayList<Node> actions = pChildren.get(l).getChildren();
		    							int ia = (int) (Constants.uniformDouble(0, actions.size()-1)+0.5);
		    							int a = actions.get(ia).data;
		                				interaction.add(a);
		                			}//*/
		                			
		                			//based on perception of cell properties
		                			//value is integer between 0:coarse
		                			double ind_prop = c.getProp(k);
		                			
		                			if( (value == (int) (Constants.PropGrain*ind_prop+0.5)) ){
		                				//record interaction
		                				interacting.add(i);
		                				interactedOn.add(m);
		                				interaction.add(act);
		                				numberActions++;
		                			}
		                		}
							} else if (act>6) {
								//bad way of dealing with this
								//7 = n phys prop
								int kk = (int) ((k/5.0)+0.5);
								double phy_prop = c.getPhy(kk);
								if( (value != (int) (Constants.PropGrain*phy_prop+0.5)) ){
									c.changeProperties(act);
									//not used??
	                				numberActions++;
								}
							}
						}
                	}
                	
                }
        		
        		if(moved){
        			newPositions.add(np[0]);
        			newPositions.add(np[1]);
        			moving.add(creature);
        		}
        		//- numberActions*Constants.ActionCost;
            }    
        }
		
		//distribute energy to symbionts, update position for free
		for (int i = 0; i < size; i++) {
			EmbodiedIndividual creature = c.creatures.get(i);
			EmbodiedIndividual topS = creature.topSymbiont;
			if(topS!=null){
				int index = -1;
				if((index = moving.indexOf(topS))>=0){
					moving.add(creature);
					double cx = newPositions.get(index*2);
					double cy = newPositions.get(index*2+1);
					newPositions.add(cx);
					newPositions.add(cy);
				}
			}
			double part_ene = 0;
			for (Iterator<EmbodiedIndividual> iterator = creature.from_symbionts.iterator(); iterator.hasNext();) {
				EmbodiedIndividual sym = iterator.next();
				part_ene += sym.getPartialEnergy();
			}
			if(creature.from_symbionts.size()>0){
				//mlog.say("symbionts "+creature.from_symbionts.size());
				//mlog.say("got bonus "+part_ene);
				creature.setEnergy(creature.getEnergy()+part_ene);//+0.2);
			}
		}
				
		
		
		//sort out the interactions (order should be random)
		ArrayList<Integer> randomList = new ArrayList<Integer>();
		for(int i=0;i<interacting.size();i++){
			randomList.add(i);
		}
		Collections.shuffle(randomList);
		
		for(int j=0;j<randomList.size();j++){
			int i = randomList.get(j);
			//is interaction still valid?
			if((interacting.get(i)<0) || (interactedOn.get(i)<0)){
				continue;
			}
		
			//names not good
			int predator = interacting.get(i);
			int prey = interactedOn.get(i);
			EmbodiedIndividual cpred = c.creatures.get(predator);
			EmbodiedIndividual cprey = c.creatures.get(prey);
			
			//if "eat", delete prey and turn predator to black
			int inter = interaction.get(i);
			if(inter == Constants.ActEat){
				//hunger-like
				if(cpred.getEnergy()>cpred.getMatForKids()){
					continue;
				}
				//dont eat collaborators
				if(cpred.from_symbionts.contains(cprey)){
					continue;
				}
				
				//are energies compatible with this?
				double ok = 0;
				ok = cpred.getEnergy() - cprey.getEnergy();

				if(cprey.topSymbiont!=null){
					ok = cpred.getEnergy() - cprey.topSymbiont.getEnergy();
				}
				if(ok>=0){					
					//delete prey from arrays
					Collections.replaceAll(interacting, prey,-1);
					Collections.replaceAll(interactedOn, prey,-1);
					if(!remove.contains(prey)){
						remove.add(cprey);
					}
					//give energy to predator
					double e = cprey.getEnergy();
					if(e>0){
						double energy = cpred.getEnergy() + e;
						cpred.setEnergy(energy); 
						cpred.eatenPlus();
						if(!cprey.parentIsLight()){
							cpred.comEatenPlus();
						}
						//record prey as dead
						cprey.setEnergy(0); 
						cprey.setEaten(1);
						cpred.setBorderColor(Color.black);
					}
				} else if(ok<=0){
					/*double ePred = cpred.getEnergy();
					double ePrey = cprey.getEnergy();
					//wound predator
					/*double energy = ePred-ePrey*Constants.ErrorCost;
					c.creatures.get(predator).setEnergy(energy);
					//wound prey 
					energy = ePrey-ePred*Constants.ErrorCost*3;
					c.creatures.get(prey).setEnergy(energy);
					//mlog.say("died "+ok);*/
					cpred.setBorderColor(Color.red);
					cprey.setBorderColor(Color.gray);
					//mlog.say("wounded "+ c.creatures.get(predator).energy);
				}
			} else if(inter == Constants.ActIntegrate){
				//put in array
				//but there can be many layers of symbionts...
				if(cpred.from_symbionts.size()<cpred.n_symbionts && cprey.topSymbiont==null && !cpred.from_symbionts.contains(cprey)){
					mlog.say("integrated");
					cpred.from_symbionts.add(cprey);
					cprey.to_symbionts.add(cpred);
					cprey.count_to_symbionts = cprey.count_to_symbionts+1;
					if(cpred.getMaxEnergy()>=cprey.getMaxEnergy()){
						cprey.topSymbiont = cpred;
					}else{
						cpred.topSymbiont = cprey;
					}
				}
			}
		}
	}


	/** updates motion and deaths*/
	public void updateMoved(){
		time++;
		
		updateDead();
		updateMotions();
		addBabies();
		
		//for dead ones
		remove = new LinkedList<EmbodiedIndividual>();
		//allow UI to update
		d.updateComponents();
	}
	
	private void addBabies() {
		//add new babies
	//	mlog.say("babies "+ babies.size());
		for (int i = 0; i < babies.size(); i++) {
			EmbodiedIndividual baby = babies.get(i);
			double[] position = baby.getPosition();
			globalID++;
			baby.setID(globalID);
			//mlog.say("added to map");
			int nx = (int) (position[0]+0.5);
			int ny = (int) (position[1]+0.5);
			addIndividual(nx, ny, baby);
			if(!baby.parentIsLight()){
				d.addComponent(baby);
			}
        }	
		//for new ones
		//babies.clear();
		babies = new LinkedList<EmbodiedIndividual>();
	//	mlog.say("babies "+ babies.size());

	}
	
	private void updateMotions() {
		//update moved
		for (int i = 0; i < moving.size(); i++) {
			int pos = remove.indexOf(i);
			if(pos>=0){
				continue;
			}
			EmbodiedIndividual creature = moving.get(i);
			//if(!remove.contains(creature)){
				double[] position = creature.getPosition();
				//new x,y	
				double x = newPositions.get(i*2);
				double y = newPositions.get(i*2+1);
				int nx = (int) (x+0.5);
				int ny = (int) (y+0.5);
				if(nx==50 || ny==50){
					//mlog.say("x "+newPositions.get(i*2)+ " y "+newPositions.get(i*2+1));
				}
				/*double nx = newPositions.get(i*2);
				double ny = newPositions.get(i*2+1);*/
				//if((position[0] !=nx) || ( position[1] !=ny)){//this simple comparison took forever?????
					updatePosition(nx,ny,creature);
					position[0] = x;
					position[1] = y; 	
					creature.setPosition(position);
				//}
			//}
        }
		
		moving = new LinkedList<EmbodiedIndividual>();
		newPositions = new LinkedList<Double>();

	}
	
	private void updateDead() {
		//update dead
		for(int i=0; i<remove.size();i++){
			EmbodiedIndividual creature = remove.get(i);
			if(!creature.isLight() & !creature.parentIsLight()){
				//write down info
				// "ID,parent,created,lifeSpan,speed,maxEnergy,kidEnergy,sensors,ancestor\n";
				String str = creature.stringDesc();
				try {
					summaryWriter.append(str);
					summaryWriter.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}///*

			//remove from display
			if(!creature.parentIsLight()){
				d.removeComponent(creature);
			}
	    	//remove from map
	    	removeIndividual(creature);
	    	
	    	//remove from symbionts
	    	for (Iterator<EmbodiedIndividual> iterator = creature.to_symbionts.iterator(); iterator.hasNext();) {
				EmbodiedIndividual sy = iterator.next();
				sy.from_symbionts.remove(creature);
			}
	    	for (Iterator<EmbodiedIndividual> iterator = creature.from_symbionts.iterator(); iterator.hasNext();) {
				EmbodiedIndividual sy = iterator.next();
				sy.to_symbionts.remove(creature);
				sy.count_to_symbionts = sy.count_to_symbionts-1;
			}
		}	
	}
	
	public int incrementGlobalID(){
		globalID++;
		return globalID;
	}
	
	/**
	 * a cell on the map
	 * should have own class file.
	 * @author lana
	 *
	 */
	public class Cell{
		//cell's physical properties
		/** how easy light goes through it (0=does not get out)*/
		double transparency = 1;
		/** how easy it is to move through (1=cannot move) */
		double density = 0;//TODO use. may also change how sound etc travels.
		
		double ntransparency = 1;
		double ndensity = 0;//TODO use. may also change how sound etc travels.
		
		/** determined by animals and transparency on this cell*/
		double luminosity;
		double sound;
		double smell;
		double temperature;
		double electric;
		
		/** all creatures on this cell*/
		LinkedList<EmbodiedIndividual> creatures;
		
		public Cell(){
			creatures = new LinkedList<EmbodiedIndividual>();	
		}
		
		public double getPhy(int kk) {
			double p = 0;
			switch (kk) {
			case 0:{
				p = transparency;
				break;
			}
			case 1:{
				p = density;
				break;
			}
			default:
				break;
			}
			return p;
		}

		/** return a property of the cell*/
		public double getProp(int k) {
			double r = 0;
			//todo put all in an array
			switch (k) {
			case 0:
				r = luminosity;
				break;
			case 1:
				r = sound;
				break;
			case 2:
				r = smell;
				break;
			case 3:
				r = temperature;
				break;
			case 4:
				r = electric;
				break;
			default:
				r = 0;
				break;
			}
			return r;
		}

		public void changeProperties(int action) {
			/*switch (action) {
			case Constants.LessTransparency:{
				changeProperties(-0.01,0);
				break;
			}
			case Constants.MoreTransparency:{
				changeProperties(0.01,0);
				break;
			}
			case Constants.LessDensity:{
				changeProperties(0,-0.01);
				break;
			}
			case Constants.MoreDensity:{
				changeProperties(0,0.01);
				break;
			}
			default:
				break;
			}*/
		}
		/**
		 * 
		 * @param t transparency
		 * @param d density
		 */
		private void changeProperties(double t, double d){
			ntransparency+=t;
			ndensity+=d;
			
			ntransparency = check(ntransparency, 0, 1);
			ndensity = check(ndensity, 0, 1);
		}
		
		
		/**
		 * reset animal dependent values to 0
		 * and recalculate them
		 */
		public void calculateValues(){
			//smell and temp could last longer in time
			luminosity = 0;
			sound = 0;
			smell = 0;
			temperature = 0;
			electric = 0;
			
			//ntransparency = transparency;
			//ndensity = density;
			transparency = ntransparency;
			density = ndensity;
			
			//make modular function for this
			for (Iterator<EmbodiedIndividual> iterator = creatures.iterator(); iterator.hasNext();) {
				EmbodiedIndividual ind = iterator.next();
				luminosity+=ind.getLuminosity();
				sound += ind.getLoud();
				smell += ind.getSmelly();
				temperature += ind.getWarm();
				electric += ind.getElectric();
			}

			luminosity = luminosity/creatures.size();
			sound = sound/creatures.size();
			smell = smell/creatures.size();
			temperature = temperature/creatures.size();
			electric = electric/creatures.size();
			
			luminosity = Constants.shiftMax(luminosity, transparency);
			sound = Constants.shiftMax(sound, density);
			smell = Constants.shiftMax(smell, 1-transparency);
			temperature = Constants.shiftMax(temperature, 1-density);
			electric = Constants.shiftMax(electric, density*transparency);
		}
	}
	

	
	//set at bounds
	private double check(double val, double low, double high) {
		if(val<low) val = low;
		if(val>high) val = high;
		return val;
	}
	
	private boolean generateBool(){
		boolean b = false;
		if(Constants.uniformDouble()>0.5){
			b = true;
		}
		
		return b;
	}
}
