package communication;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import startup.Constants;

public class FileBuilder {
	MyLog mlog = new MyLog("fileBuilder",true);
	/**File writer*/
	FileWriter filew;
	
	public FileBuilder(){
		
		/**data directory*/
		String folderName;

		
		//get current date
	    DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm");
	    Date date = new Date();
	    String strDate = dateFormat.format(date);
	
//	    if((name.compareTo("")==0)){
	    	folderName = Constants.DataPath + "/" + strDate + "/";
//	    }else{
//	    	folderName = Constants.DataPath + "\\" +name + "\\";
//	    }
	    	
    	//first create directory
		File theDir = new File(folderName);
		// if the directory does not exist, create it
		if (!theDir.exists()) {
		    mlog.say("creating directory: " + folderName);
		    boolean result = false;

		    try{
		        theDir.mkdirs();
		        result = true;
		    } 
		    catch(SecurityException se){
		        //handle it
		    }        
		    if(result) {    
		        System.out.println("DIR created");  
		    }
		}
		
		//now create csv files
		try {			
			filew = new FileWriter(folderName+"/"+ Constants.SummaryFileName);
			mlog.say("stream opened "+ Constants.SummaryFileName);
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	public FileWriter getFileWriter(){
		return filew;
	}

}
