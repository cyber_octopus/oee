/**
 * 
 */
package startup;

import java.util.Random;

/**
 * @author lana
 * Class containing all the constants variable and generic functions.
 */
public class Constants {
	/** update graphics */
	//TODO make a better UI!
	public static boolean draw = true;
	/** percentage of elements drawn*/
	public static double draw_coarse = 1;
	
	/** equivalent to refresh rate */
	public static int refresh_rate = 500;


	
	/** grid limits*/
	public static final int GridMax = 50;// 10+40+10;//20+20+10+(security)10 
	public static final int GridStep = 20;
	
	/** actions*/
	public static final int ActEat = 0;
	public static final int ActMate = 1;
	/** take as symbiont*/
	public static final int ActIntegrate = 2;
	
	public static final int MoveUp = 3;
	public static final int MoveDown = 4;
	public static final int MoveRigh = 5;
	public static final int MoveLeft = 6;
	
	public static final int MoreTransparency = 7;
	public static final int LessTransparency = 8;
	public static final int MoreDensity =9;
	public static final int LessDensity = 10;
	
	public static final int ActionTypes = 11;



	
	// factors on property values
	/** mutation factor*/
	public static final double MutFactor = 0.2;
	/** speed*/
	public static final double SpeedFactor = 0.3;//0.02
	/** absolute max value*/
	public static final int SpeedMax= 10;
	/** absolute max value*/
	public static final int EnergyMax = 1000;
	/** coarse graining of property values*/
	public static final int PropGrain = 100;

	//costs
	public static final double SpeedCost= 0.001;//0.2
	/** cost of being alive */
	public static final double StepCost = 0.002;//
	/** cost of having sensors*/
	public static final double SensorCost = 0.2;//0.05
	/**cost of making sensory errors, ratio of prey energy*/
	public static final double ErrorCost = 0.2;
	/** energy iput into system */
	public static final double FreeEnergy =1;//3
	
	/** how far the kids are from the parents*/
	public static final double BirthDistance = 1.5;//1.5
	/** how far the kids are from the parents*/
	public static final double LightBirthDistance = 1.5;

	
	//public static final int EnergyTypes = 2;
	
	/** folder where data file will be recorded*/
	public static String DataPath = "/Users/lana/Development/new_OEE_data";
	public static double ActionCost = 0.5;
	/** files*/
	public static final String SummaryFileName = "SummaryIndividuals.csv";
	
	
	 // NOTE: Usually this should be a field rather than a method
    // variable so that it is not re-seeded every call.
    static Random rand = new Random();

	
	/**
	 * from http://stackoverflow.com/questions/363681/generating-random-integers-in-a-range-with-java
	 * and http://stackoverflow.com/questions/3680637/how-to-generate-a-random-double-in-a-given-range
	 * Returns a pseudo-random number between min and max, inclusive.
	 * Uniform distribution.
	 * The difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min Minimum value
	 * @param max Maximum value.  Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 */
	public static double uniformDouble(double min, double max) {

	    // NOTE: Usually this should be a field rather than a method
	    // variable so that it is not re-seeded every call.
	    //Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    double randomNum = min + (max - min) * rand.nextDouble();
	    //rand.nextInt(max - min) + 1)

	    return randomNum;
	}
	
	/**
	 * Uniform distribution between 0 and 1
	 * @return random number
	 */
	public static double uniformDouble() {
		double min = 0;
		double max = 1;
	   
	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    double randomNum = min + (max - min) * rand.nextDouble();
	    //rand.nextInt(max - min) + 1)

	    return randomNum;
	}
	
	
	/**
	 * shifts a number so values closest to max are 1
	 * @param m between 0..1
	 * @return
	 */
	public static double shiftMax(double val, double m) {
		double s = 1-Math.abs(m-val);//triangular
		s = checkZero(s, m-0.2, m+0.2);//obelisk
		return s;
	}
	
	//sets at 0 if out of bounds
	private static double checkZero(double val, double low, double high) {
		if(val<low) val = 0;
		if(val>high) val = 0;
		return val;
	}
}

