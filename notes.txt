no cost on eating error  because... ?


no distinct species despite diff transparency because?
1- no actual advantage from speed, treated as neutral mutation. Becase food on 1 cell is enough (density too high, or too many sensors.)
	would still show a divide between 2 spaces but maybe not visible on tree because many different speeds in space 1
2- cost too progressive, no difference for different level of speeds -> see above

1: record creatures position at death and compare.
# principal prey must be rare (record what eaten proportion was light?). Reduce free energy (A) and see * to make dependance on source food happen.
Make sensors more expensive (B)
Make properties more distributed if possible (C)

2: make it quadratic
* Don't forget what we really want is them chasing each other. More important than niches. Can only happen with sensor race (ie species don't have ALL the sensor so others can mutate to escape them. Predator pressure must be strong enough that escape does provide an advantage > to the cost of changing.)

A. Half successful. got 2 "sprouts" pgDeath vs matForKids but they stayed even when speed == 0. speed really only neutral mutaition.
B. Got dependency on 1,2 sensors. "luminosity" value mostly the same for everyone.

---

Introduced hunger to deal with immediate depletion of preys for complex prey eaters.
-> this made speed a non-neural, non-negative mutation at last

TODO
proper ui with speed.

--- possible that we obtained sensor race but difficult to see. Now going to parasitism.
Exponential cost and linear input means

1. we need to make sensor combination more profitable than simple addition (lower cost or higher return).

OR

2. Exploit the free energy of simple creatures. If cost is exponential, there must be wasted energy near the origin of the curve. Hence parasitism/symbiosis, but inverse of what is usually pictured: big guys "parasiting" (feeding on waste) of small guys. 


It seems that symbionts kill each other because their sensors arent specialized. Or is it that the killed symbiont always spring back to life?

should there be a price on the nr of symbionts? Probably not necessary since energy in whole system is limited anyway. (rule for deciding if there is cost or not?)


http://www.tns-sofres.com/publications/limage-de-la-france-dans-le-monde

